import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DashboardPage } from './dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardPage,
    children:
    [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('src/app/tab1/tab1.module').then(m => m.Tab1PageModule)
          }
        ]
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('src/app/tab2/tab2.module').then(m => m.Tab2PageModule)
          }
        ]
      },
      {
        path: 'tab3',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('src/app/tab3/tab3.module').then(m => m.Tab3PageModule)
          }
        ]
      },

      {
        path: 'tab4',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../../tab4/tab4.module').then(m => m.Tab4PageModule)
          }
        ]
      },

      {
        path: 'tab5',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../../tab5/tab5.module').then(m => m.Tab5PageModule)
          }
        ]
      },
      {
        path: 'tab6',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../../tab6/tab6.module').then(m => m.Tab6PageModule)
          }
        ]
      },

      {
        path: '',
        redirectTo: 'tab1',
        pathMatch: 'full'
      }
    ]
  },


  {
    path: '',
    redirectTo: '/dashboard/tab1',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DashboardPage]
})
export class DashboardPageModule {}
