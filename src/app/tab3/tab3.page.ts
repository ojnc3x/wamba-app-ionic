import { Component } from '@angular/core';
import { ModalController, NavController, LoadingController } from '@ionic/angular';
//import { HttpClient, Headers, RequestOptions } from '@angular/http';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { SuccessPage } from '../modal/success';
import { ErrorPage } from '../modal/error';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
  providers: [SuccessPage, ErrorPage]
})

export class Tab3Page {
  private contact_message : FormGroup;
  status_code;
  modal_status;
  leparams;
  showModal: boolean;


  constructor(private formBuilder: FormBuilder, public modalCtrl: ModalController ) {
    this.contact_message = this.formBuilder.group({
      code: ['', Validators.required],

    });
  }
  ContactForm(){
    this.showModal = true;

    /*
    let loader = this.loading.create({
      content: "Por favor espere...",
    });

    loader.present().then(() => {
      console.log(this.contact_message.value);

      let headers = new Headers({ 'Content-Type': 'application/json; charset=UTF-8' });

      let options = new RequestOptions({ headers: headers });


      let postParams =
      {
        contact_message: {
          "name": this.contact_message.value.name,
          "email": this.contact_message.value.email,
          "phone": this.contact_message.value.number,
          "comment": this.contact_message.value.comment
        }
      }

      console.log(postParams);

      this.http.post("http://reencuentro.co/api/v1/contacts.json", postParams, options)
        .subscribe(data => {
          this.status_code = data.json().status;

          if(this.status_code == "201"){
            //this.modal_status = SuccessPage;
            //this.openModal(this.modal_status);
            //this.contact_message.reset();
          }
          else{
            //this.modal_status = ErrorPage;
            //this.openModal(this.modal_status);
          }

         }, error => {
          console.log(error);// Error getting the data
        });



      loader.dismiss();
    });
    */
  }
  hide()
    {
      this.showModal = false;
    }
    
  openModal(status) {
    let modal = this.modalCtrl.create(status);

  }

}


/*
export class HomePage {
  constructor(public navCtrl: NavController, public http: Http) {
  }

  postRequest() {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postParams = {
      title: 'foo',
      body: 'bar',
      userId: 1
    }

    this.http.post("http://jsonplaceholder.typicode.com/posts", postParams, options)
      .subscribe(data => {
        console.log(data['_body']);
       }, error => {
        console.log(error);// Error getting the data
      });
  }
}

Processing by Api::InformationsController#contacts as JSON
  Parameters: {"contact_message"=>{"name"=>"asfsfasf", "email"=>"dafas@sdadas.c", "content"=>"asfsdfsdfasd"},
  "subdomain"=>"api", "information"=>{"contact_message"=>{"name"=>"asfsfasf", "email"=>"dafas@sdadas.c", "content"=>"asfsdfsdfasd"}}}


*/
