import { Component } from '@angular/core';
import { ModalController, Platform, NavParams, NavController } from '@ionic/angular';

@Component({
  templateUrl: 'success.html'
})

export class SuccessPage {
  character;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: NavController,
    public modal: ModalController
  ) {

  }

  dismiss() {
    this.modal.dismiss();
  }
}
