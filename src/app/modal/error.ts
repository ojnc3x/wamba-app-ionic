import { Component } from '@angular/core';
import { ModalController, Platform, NavParams, NavController } from '@ionic/angular';;

@Component({
  templateUrl: 'error.html'
})

export class ErrorPage {
  character;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: NavController,
    public modal: ModalController
  ) {

  }

  dismiss() {
    this.modal.dismiss();
  }
}
